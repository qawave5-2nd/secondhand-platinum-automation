Feature: Check login functionality
  Users can log in using the registered password and email
  
  Rule: Only registered users can log in

  Scenario: Only checked logins are successful with valid email and password
    Given I am open the website
    When I go to home page
    And I click masuk button
    And I input email
    And I input password
    And I click masuk button
    Then I is navigated to the home page
