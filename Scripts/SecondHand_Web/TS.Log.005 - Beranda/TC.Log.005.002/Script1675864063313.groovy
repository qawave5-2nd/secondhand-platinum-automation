import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Open Browser And Go to The Url'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-General/Start Open URL'), null, FailureHandling.STOP_ON_FAILURE)

'Go to Login And Logged In'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-General/Login Temporary'), null, FailureHandling.STOP_ON_FAILURE)

'Click And Verify Daftar Jual'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Click And Verify Icon Daftar Jual'), null, FailureHandling.STOP_ON_FAILURE)

'Click And Verify Notification'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Click And Verify Icon Notification'), null, FailureHandling.STOP_ON_FAILURE)

'Click And Verify Profile'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Click And Verify Icon Profile'), null, FailureHandling.STOP_ON_FAILURE)

'Close Browser'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-General/Close Browser'), null, FailureHandling.STOP_ON_FAILURE)
