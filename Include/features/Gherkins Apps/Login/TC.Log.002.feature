Feature: Check Login Functionality
User able to input data when Login.

Rule: Only using registered email.

	Scenario : User want to login with registered email.
		Given I am launch the app
		When I go to login page
		And I input user data
		And I input Email "nurulbasoka1@mail.com"
		And I input Password "asdasdasd"
		And I click daftar button
		Then I see Home Page
		
		
	Scenario : User want to login with unregistered email.
		Given I am launch the app
		When I go to login page
		And I input user data
		And I input Email "nurulbasoka11@mail.com"
		And I input Password "asdasdasd"
		And I click daftar button
		Then I see message <Email atau kata sandi salah>
		
		
	Scenario : User want to login but not input email field.
		Given I am launch the app
		When I go to login page
		And I input user data
		And I input Password "asdasdasd"
		And I click daftar button
		Then I see message <Email tidak boleh kosong>
		
		
	Scenario : User want to login but not input email.
		Given I am launch the app
		When I go to login page
		And I input user data
		And I input Email "nurulbasoka11@mail.com"
		And I click daftar button
		Then I see message <Password atau kata sandi salah>
		
		