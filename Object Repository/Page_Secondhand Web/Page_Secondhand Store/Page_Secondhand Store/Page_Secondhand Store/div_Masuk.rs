<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Masuk</name>
   <tag></tag>
   <elementGuidId>9350b2a6-3cdc-4ad3-a278-e96e57041e93</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.container</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/nav/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4c297ac1-6b31-425e-ab45-563282692379</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container</value>
      <webElementGuid>5d52db49-8560-4a33-81ac-6f198115edf1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Masuk</value>
      <webElementGuid>5fb1f46c-3f56-4645-9753-30343a653a79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/nav[@class=&quot;navbar navbar-expand-lg sticky-top&quot;]/div[@class=&quot;container&quot;]</value>
      <webElementGuid>3188a4ba-9dd9-4e0e-a578-aa0372aa1b5d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/nav/div</value>
      <webElementGuid>22fd8100-3bc5-4e36-ab99-3ed7933fafcc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulan Ramadan Banyak Diskon!'])[1]/preceding::div[3]</value>
      <webElementGuid>c64e0ecb-0d2c-4f26-8686-9238cf1b4106</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/div</value>
      <webElementGuid>0c2d392d-3a56-4356-afa9-c8d0777ee3d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' Masuk' or . = ' Masuk')]</value>
      <webElementGuid>c99b4739-c8eb-4cd2-9eee-3031d2c71114</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
