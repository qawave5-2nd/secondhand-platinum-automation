import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'User Click widget beranda'
Mobile.tap(findTestObject('Object Repository/Secondhand_App/Beli Functionality/android.widget.ImageView (1)'), 0)
'User Click widget cari'
Mobile.tap(findTestObject('Object Repository/Secondhand_App/Beli Functionality/android.widget.EditText - Cari di Second Chance'),
	0)
'user input item name = paimon'
Mobile.setText(findTestObject('Object Repository/Secondhand_App/Beli Functionality/android.widget.EditText - Cari di Second Chance (1)'),
	'paimon', 0)
'system view item paimon'
Mobile.tap(findTestObject('Object Repository/Secondhand_App/Beli Functionality/android.view.ViewGroup'), 0)
'user click button saya tertarik dan ingin nego'
Mobile.tap(findTestObject('Object Repository/Secondhand_App/Beli Functionality/android.widget.Button - Saya Tertarik dan Ingin Nego'),
	0)
'user input nego price'
Mobile.setText(findTestObject('Object Repository/Secondhand_App/Beli Functionality/android.widget.EditText - Rp 0,00'),
	'90', 0)
'user click button kirim'
Mobile.tap(findTestObject('Object Repository/Secondhand_App/Beli Functionality/android.widget.Button - Kirim'), 0)
'nego offered to seller'