<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Penawaran Produk_svg-inline--fa fa-bell fa-lg</name>
   <tag></tag>
   <elementGuidId>ae3b3239-c9db-4217-bbba-ebad324fd873</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran Produk'])[1]/preceding::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>a2c111ae-9e2e-44ed-a472-7efd3153c73d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>05730c4f-5805-4e4e-a7a6-d4c5957bb506</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e974e073-b8df-43d3-9c7c-95c4f3f04e44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-prefix</name>
      <type>Main</type>
      <value>fas</value>
      <webElementGuid>9963bedd-52ca-4751-b7f8-7d9aa09f5617</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-icon</name>
      <type>Main</type>
      <value>bell</value>
      <webElementGuid>56cfc688-40d6-48be-a636-4de8cd2e99b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>svg-inline--fa fa-bell fa-lg </value>
      <webElementGuid>94a17c4c-8502-4a52-826d-fde0407f8313</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>42bce2b2-5fa3-4b1f-83d7-96e1928a18fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>55eab263-1d56-493e-be59-fe12abad8793</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 448 512</value>
      <webElementGuid>82dde893-7456-4d5d-bc4b-eb1d30db4d3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;notification&quot;)/svg[@class=&quot;svg-inline--fa fa-bell fa-lg&quot;]</value>
      <webElementGuid>a2743dfc-ea34-414d-86ed-104341c64f89</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran Produk'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>aa5afe16-0b18-4704-a084-267f790be715</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='motor'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>e75be1cc-ce3a-4111-8195-1a632dcb7306</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
