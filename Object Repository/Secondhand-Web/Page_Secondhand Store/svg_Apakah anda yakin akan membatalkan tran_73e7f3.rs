<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Apakah anda yakin akan membatalkan tran_73e7f3</name>
   <tag></tag>
   <elementGuidId>2467cd36-4d07-4374-8219-53738cd417b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Apakah anda yakin akan membatalkan transaksi ini?'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>1b91ef89-f447-42de-8030-d2617f7cfd03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a5c75cb1-b867-421f-8d6c-f78244e12d1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>71c5a504-78ed-4c32-ac98-4959be4a45d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-prefix</name>
      <type>Main</type>
      <value>fas</value>
      <webElementGuid>8da2fe65-5e0b-41e4-ad3c-2742188aabc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-icon</name>
      <type>Main</type>
      <value>xmark</value>
      <webElementGuid>e7e4fd91-a0b3-4a01-86b4-0ba9fd0d3060</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>svg-inline--fa fa-xmark </value>
      <webElementGuid>5cad09e2-a957-4c60-90f2-17dc97ac93ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>37115a28-86a3-488a-bc28-05807c25633e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>793b9833-d21f-42f2-9e37-a8cefcdc5f24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 320 512</value>
      <webElementGuid>84a9b716-8ae5-4cea-8b8f-08d8fe77c6ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog modal-sm modal-dialog-centered&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]/button[@class=&quot;btn btn-light&quot;]/svg[@class=&quot;svg-inline--fa fa-xmark&quot;]</value>
      <webElementGuid>03756175-7823-4abd-88dd-303fad842da6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apakah anda yakin akan membatalkan transaksi ini?'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>ff6ec510-b288-44fd-a7ae-bfeca2d1b270</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terima'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>ff42525b-782f-4141-a2ff-620823229d4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Iya'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>711ba7a2-7f5c-46b4-a1ef-4761e7ab08ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tidak'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>c8b47418-0d4c-45de-9762-d7c60cfbbdad</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
