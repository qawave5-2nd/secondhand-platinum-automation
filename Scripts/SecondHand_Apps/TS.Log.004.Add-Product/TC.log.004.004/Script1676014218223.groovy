import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'start application'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-AddProduct/start apk'), null, FailureHandling.STOP_ON_FAILURE)

'go to login page'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Login/Go to Login Page'), null, FailureHandling.STOP_ON_FAILURE)

'input login data'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Login/Input Data Login'), null, FailureHandling.STOP_ON_FAILURE)

'click on login button'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Login/Click Button Login'), null, FailureHandling.STOP_ON_FAILURE)

'click on tambah produk icon button'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-AddProduct/click on tambah produk icon button'), null, FailureHandling.STOP_ON_FAILURE)

'input product data'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-AddProduct/input product data'), null, FailureHandling.STOP_ON_FAILURE)

'add product picture'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-AddProduct/add product picture'), null, FailureHandling.STOP_ON_FAILURE)

'click on terbitkan'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-AddProduct/click on terbitkan button'), null, FailureHandling.STOP_ON_FAILURE)

'close application'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-General/Close Application'), null, FailureHandling.STOP_ON_FAILURE)

