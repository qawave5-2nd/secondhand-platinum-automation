<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Status produk berhasil diperbarui</name>
   <tag></tag>
   <elementGuidId>ade099db-1b10-4780-85a7-a32a75a1086d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.fade.position-absolute.top-0.start-50.translate-middle.alert.alert-success.show > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>b5f792de-c02f-4102-b47e-a8d544f30f5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Status produk berhasil diperbarui</value>
      <webElementGuid>c68055f7-eb4f-4e1c-979d-4a5d1b5751bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;fade position-absolute top-0 start-50 translate-middle alert alert-success show&quot;]/p[1]</value>
      <webElementGuid>ee027cad-2841-4e1f-adf3-f1a556b83bc1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/p</value>
      <webElementGuid>4d6f8cae-1512-4076-a277-f7a39a281b06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ditawar Rp 1.000.000,00'])[4]/following::p[1]</value>
      <webElementGuid>22cb7a3b-0b0d-4411-ad32-039d9486b192</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 1.500.000,00'])[4]/following::p[1]</value>
      <webElementGuid>0f60a695-4a10-4ad6-97f8-a9fbab6253fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Status produk berhasil diperbarui']/parent::*</value>
      <webElementGuid>496b38e3-8e40-4b75-b418-fd6a3cf48162</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/p</value>
      <webElementGuid>5b169f6f-4762-4cd4-a46c-b52ff558c9d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Status produk berhasil diperbarui' or . = 'Status produk berhasil diperbarui')]</value>
      <webElementGuid>8caf5f76-04d4-4c21-8cd5-7f3446f252cd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
