import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Open Website'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-General/Start Open URL'), null, FailureHandling.STOP_ON_FAILURE)

'Login'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Check-Order-bid/login'), null, FailureHandling.STOP_ON_FAILURE)

'Click on list button'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Check-Order-bid/click on list button'), null, FailureHandling.STOP_ON_FAILURE)

'Click on diminati list'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Check-Order-bid/click on diminati list'), null, FailureHandling.STOP_ON_FAILURE)

'Accept bid order'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Check-Order-bid/accept order'), null, FailureHandling.STOP_ON_FAILURE)

'Click on status'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Check-Order-bid/click on status button'), null, FailureHandling.STOP_ON_FAILURE)

'Choose batalkan'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Check-Order-bid/click on batalkan'), null, FailureHandling.STOP_ON_FAILURE)

'Click kirim on status page'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Check-Order-bid/click on button kirim on status page'), null, FailureHandling.STOP_ON_FAILURE)

'back to list page'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Check-Order-bid/click on back button'), null, FailureHandling.STOP_ON_FAILURE)

'click on sold item list button'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Check-Order-bid/click on terjual button'), null, FailureHandling.STOP_ON_FAILURE)

'click on all product'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Check-Order-bid/click on all product button'), null, FailureHandling.STOP_ON_FAILURE)

'click item on all product page'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Check-Order-bid/click on item in all product page'), null, FailureHandling.STOP_ON_FAILURE)

'Close Website'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-General/Close Browser'), null, FailureHandling.STOP_ON_FAILURE)

