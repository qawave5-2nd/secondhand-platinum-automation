<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>strong_Silahkan verifikasi email agar dapat_32ea4a</name>
   <tag></tag>
   <elementGuidId>820e2f61-421f-49be-8d33-b6ac6a14b43e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-success.alert-dismissible.fade.show > strong</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/form/div/strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>e4d64320-5170-4f73-a084-de8f456da296</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Silahkan verifikasi email agar dapat menggunakan layanan kami</value>
      <webElementGuid>573e9cd9-f00d-4860-a533-1029db0d86fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;App&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-6 form-register&quot;]/form[1]/div[@class=&quot;alert alert-success alert-dismissible fade show&quot;]/strong[1]</value>
      <webElementGuid>b7099a74-7c4c-4240-8a2c-8187c040bf94</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div/strong</value>
      <webElementGuid>9f3b043f-8aeb-4499-9874-66653e46faa1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar'])[1]/following::strong[1]</value>
      <webElementGuid>125a96f9-3e31-4f5c-8b22-c318e1390e0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::strong[2]</value>
      <webElementGuid>294af6e5-0be0-4991-b8dc-c58f2a5c617a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::strong[1]</value>
      <webElementGuid>e874752a-6296-4b3d-9fe5-6b89208ded73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email address'])[1]/preceding::strong[1]</value>
      <webElementGuid>c505b7e1-eb8a-46df-bd30-56e0916f9f90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Silahkan verifikasi email agar dapat menggunakan layanan kami']/parent::*</value>
      <webElementGuid>0945b453-41c5-41d4-9d3a-4d8b24342bb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/strong</value>
      <webElementGuid>66623f0a-d7b3-439a-af52-4da6eedf1220</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'Silahkan verifikasi email agar dapat menggunakan layanan kami' or . = 'Silahkan verifikasi email agar dapat menggunakan layanan kami')]</value>
      <webElementGuid>25073265-c5e1-4c04-a5cf-6a98c05b1395</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
