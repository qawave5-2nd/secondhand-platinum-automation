Feature: Account Functionality
User able to change profile info

Rule: Only registered user are able to change profile info

Scenario : User login with registered email than upload new photo profile
		Given I am open the app
		When I go to login account page
		And I click profile icon
		And I click masuk button
		And I put registered email
		And I put password
		And I put masuk button
		When I go to profile info page
		And I click profile edit icon
		When I input new profile photo
		And I click profile image
		And I choose from gallery
		And I pick the new photo
		When I want verify the new photo has updated
		Then I see pop-up notif appeared with the words 'Profil berhasil diperbarui'		

Scenario : User login with registered email than set new name
		Given I am open the app
		When I go to login account page
		And I click profile icon
		And I click masuk button
		And I put registered email
		And I put password
		And I put masuk button
		When I go to profile info page
		And I click profile edit icon
		When I input new profile name
		And I click nama field
		And I put new name
		And I click simpan button
		When I want verify the new name has updated
		Then I see pop-up notif appeared with the words 'Profil berhasil diperbarui'	
		
Scenario : User login with registered email than set new phone number
		Given I am open the website
		When I go to login account page
		And I click profile icon
		And I click masuk button
		And I put registered email
		And I put password
		And I put masuk button
		When I go to profile info page
		And I click profile edit icon
		When I input new profile phone number
		And I click phone field
		And I put new number
		And I click simpan button
		When I want verify the new phone number has updated
		Then I see pop-up notif appeared with the words 'Profil berhasil diperbarui'		
		
Scenario : User login with registered email than set new town info
		Given I am open the app
		When I go to login account page
		And I click profile icon
		And I click masuk button
		And I put registered email
		And I put password
		And I put masuk button
		When I go to profile info page
		And I click profile edit icon
		When I input new profile town info
		And I click kota field
		And I click town drop-down box
		And I choose new town
		And I click simpan button
		When I want verify the new town has updated
		Then I see pop-up notif appeared with the words 'Profil berhasil diperbarui'
		
Scenario : User login with registered email than set new email
		Given I am open the app
		When I go to login account page
		And I click profile icon
		And I click masuk button
		And I put registered email
		And I put password
		And I put masuk button
		When I go to profile info page
		And I click profile edit icon
		When I input new email
		And I click email field
		And I put new email
		And I click simpan button
		When I want verify the new email has updated
		Then I see pop-up notif appeared with the words 'Profil berhasil diperbarui'		
		
Scenario : User login with registered email than set new password
		Given I am open the app
		When I go to login account page
		And I click profile icon
		And I click masuk button
		And I put registered email
		And I put password
		And I put masuk button
		When I go to profile info page
		And I click profile edit icon
		When I input new password
		And I click password field
		And I put new password
		And I click simpan button
		When I want verify the new password has updated
		Then I see pop-up notif appeared with the words 'Profil berhasil diperbarui'			