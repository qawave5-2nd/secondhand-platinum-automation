<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Masuk</name>
   <tag></tag>
   <elementGuidId>fbd67d2d-ec78-4992-8509-fa8a104d158e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.nav-item-login > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarText']/ul/li/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>2486e48f-3f0e-4060-8f39-c6720d7653c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/login</value>
      <webElementGuid>2a8ee0cc-15b5-41f8-8fc3-5b51def5437d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Masuk</value>
      <webElementGuid>892ef0cc-be3a-47ab-bfae-6a4c141342dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarText&quot;)/ul[@class=&quot;navbar-nav me-auto mb-2 mb-lg-0&quot;]/li[@class=&quot;nav-item-login&quot;]/a[1]</value>
      <webElementGuid>e226332b-0fd9-4391-b4c3-fdf3cecefd15</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarText']/ul/li/a</value>
      <webElementGuid>8861e850-8b32-4678-97e8-87fd2f231b8b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Masuk')]</value>
      <webElementGuid>0c24b503-9acd-4c2e-a619-82e2890d78ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulan Ramadan Banyak Diskon!'])[1]/preceding::a[1]</value>
      <webElementGuid>9a398ff4-afaa-472b-9452-9a82e9d39e9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga 60%'])[1]/preceding::a[1]</value>
      <webElementGuid>2449eb0a-80cd-4b9b-9a51-51ed9cd526c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/login')]</value>
      <webElementGuid>ce9b18d5-e288-4fe1-9336-57886055f98c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a</value>
      <webElementGuid>e0fa3c36-c757-40fb-a4b6-54e72f333a9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/login' and (text() = 'Masuk' or . = 'Masuk')]</value>
      <webElementGuid>64139b53-13a9-4e38-83eb-410cf3aaa269</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
