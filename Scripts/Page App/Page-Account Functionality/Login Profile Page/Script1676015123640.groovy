import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Call Start App'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Account Functionality/Start App Page'), null, FailureHandling.STOP_ON_FAILURE)

'Klik Ikon Profil'
Mobile.tap(findTestObject('Object Repository/android.widget.ImageView'), 0)

'Klik Tombol Masuk'
Mobile.tap(findTestObject('Object Repository/android.widget.Button - Masuk'), 0)

'Input Email'
Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Masukkan email'), 'maxamilian.maclain32@foundtoo.com', 
    0)

'Input Password'
Mobile.setEncryptedText(findTestObject('Object Repository/android.widget.EditText - Masukkan password'), 'aeHFOx8jV/A=', 
    0)

'Klik Tombol Masuk'
Mobile.tap(findTestObject('Object Repository/android.widget.Button - Masuk (1)'), 0)

'Call Verify Login Success'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Account Functionality/Verify Login Success Page'), null, FailureHandling.STOP_ON_FAILURE)

