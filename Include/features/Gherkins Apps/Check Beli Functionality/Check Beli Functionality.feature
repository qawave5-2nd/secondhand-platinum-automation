Feature: Check Beli Functionality
User use search feature to buy product.

Rule: Only User after login successfull do it.

	Scenario : User use search feature to buy product. input nego price = "90".
		Given I am launch the app
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want to buy product
		And I click beranda
		And I click widget cari
		And I input item name = "paimon"
		And I click saya tertarik dan ingin nego button
		And I Input nego price = "90"
		And I click kirim button
		Then I see message "nego offered to seller"
		
	Scenario : User use search feature to buy product but not fill nego price at nego.
		Given I am launch the app
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want to buy product
		And I click beranda
		And I click widget cari
		And I input item name = "Kucing oren"
		And I click saya tertarik dan ingin nego button
		And I click kirim button
		Then I see message "Notification of nego price can't be empty appear"
		
	Scenario : User use search feature to buy product and buy producy with 8 digit number. input nego price = "10000000"
		Given I am launch the app
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want to buy product
		And I click beranda
		And I click widget cari
		And I input item name = "Kucing Oren"
		And I click saya tertarik dan ingin nego button
		And I Input nego price = "10000000"
		And I click kirim button
		Then I throwback into product description without nego being offered
		
	Scenario : User use search feature to buy product then buy with 7 digit number. input nego price = "1500000"
		Given I am launch the app
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want to buy product
		And I click beranda
		And I click widget cari
		And I input item name = "Kucing Stiker"
		And I click saya tertarik dan ingin nego button
		And I Input nego price = "1500000"
		And I click kirim button
		Then I see message "Nego price offered"
		
		