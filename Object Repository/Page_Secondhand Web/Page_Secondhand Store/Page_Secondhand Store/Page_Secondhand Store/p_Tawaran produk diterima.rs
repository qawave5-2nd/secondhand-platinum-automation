<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Tawaran produk diterima</name>
   <tag></tag>
   <elementGuidId>01ed9199-26a6-484b-aae2-5a6ee39861a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.fade.position-absolute.top-0.start-50.translate-middle.alert.alert-success.show > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>6e1a5b6c-6132-4ca0-ae4e-40a91c4ab01c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tawaran produk diterima</value>
      <webElementGuid>2590766f-85e1-468f-b881-29fd3d3e5f79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;fade position-absolute top-0 start-50 translate-middle alert alert-success show&quot;]/p[1]</value>
      <webElementGuid>e78e200a-3aaf-4adf-8e55-d109a6874d0b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/p</value>
      <webElementGuid>1ef98991-45d5-4135-aa66-5172bc564862</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hubungi di'])[1]/following::p[1]</value>
      <webElementGuid>7e1c744f-e88d-460c-96b7-df503c47e2cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/following::p[1]</value>
      <webElementGuid>4db064ac-62bf-4fc7-bfae-6a543a6c7721</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Tawaran produk diterima']/parent::*</value>
      <webElementGuid>a95bd24a-3a50-4c75-874a-7d686ac0572c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/p</value>
      <webElementGuid>d3524722-287a-44d6-b86e-b2b370748915</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Tawaran produk diterima' or . = 'Tawaran produk diterima')]</value>
      <webElementGuid>36993e60-205d-4788-a606-f35137177460</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
