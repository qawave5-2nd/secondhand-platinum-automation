Feature: Buy_item 
User buy item after login.

Rule: Only User after login successfull and Enter nego price with any number will be success.

	Scenario : User buy item after login. with 0 rupiah as payment
		Given I am open the website
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want buy item
		And I click Product sarung
		And I click saya tertarik dan ingin nego button
		And I Input harga tawar "0"
		And I click kirim button
		Then I see the page unresponsive
		
	Scenario : User buy item after login. and nego the product below the current price but not 0. nego price = "1"
		Given I am open the website
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want buy item
		And I click Product sarung
		And I click saya tertarik dan ingin nego button
		And I Input harga tawar "1"
		And I click kirim button
		Then I see the page unresponsive
		
	Scenario : User buy item after login. and nego the product below the current price but not 0. nego price = "10"
		Given I am open the website
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want buy item
		And I click Product sarung
		And I click saya tertarik dan ingin nego button
		And I Input harga tawar "10"
		And I click kirim button
		Then I see the page unresponsive
		
	Scenario : User buy item after login. and nego the product below the current price but not 0. nego price = "100"
		Given I am open the website
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want buy item
		And I click Product sarung
		And I click saya tertarik dan ingin nego button
		And I Input harga tawar "100"
		And I click kirim button
		Then I see the page unresponsive
		
	Scenario : User buy item after login. and nego the product below the current price but not 0. nego price = "1000"
		Given I am open the website
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want buy item
		And I click Product sarung
		And I click saya tertarik dan ingin nego button
		And I Input harga tawar "1000"
		And I click kirim button
		Then I see the page view message nego offered
		
	Scenario : User buy item after login, but did not input any amount on nego the product.
		Given I am open the website
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want buy item
		And I click Product sarung
		And I click saya tertarik dan ingin nego button
		And I click kirim button
		Then I see the page unresponsive
		
	Scenario : User buy item after login, and input nego amount greater than the product price. input price = "100000000"
		Given I am open the website
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want buy item
		And I click Product sarung
		And I click saya tertarik dan ingin nego button
		And I Input harga tawar "100000000"
		And I click kirim button
		Then I see the page view message nego offered
	
	Scenario : User reload pages at buy page.
		Given I am open the website
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I want reload buy page
		And I click Product turtleboy
		And I click saya tertarik dan ingin nego button
		And I refresh the page
		Then I see the page become blank
		
	Scenario : User click back at buy page.
		Given I am open the website
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I click back at buy page
		And I click Product turtleboy
		And I click back
		Then I back to home page
		
	Scenario : User click home page at buy page.
		Given I am open the website
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I click back at buy page
		And I click Product turtleboy
		And I click Home Page
		Then I back to home page
		