<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_</name>
   <tag></tag>
   <elementGuidId>1d27c1ef-4aa7-4809-95d6-327e14a4689e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.navbar-brand</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/nav/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e63dd11c-e4db-4ebd-a2a7-16fe4e82aa7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navbar-brand</value>
      <webElementGuid>f6481042-c971-477c-8bba-1cfe711690bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/</value>
      <webElementGuid>c687f5fc-f197-47df-9e40-b18f43c0dc45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> </value>
      <webElementGuid>3f5b3ba8-b339-4752-b03d-0fed492bccef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/nav[@class=&quot;navbar navbar-expand-lg sticky-top&quot;]/div[@class=&quot;container&quot;]/a[@class=&quot;navbar-brand&quot;]</value>
      <webElementGuid>889dec13-3896-4042-acb2-304888187150</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/nav/div/a</value>
      <webElementGuid>d27ae7ff-b750-453d-b56b-6f58c8e0d024</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran Produk'])[1]/preceding::a[2]</value>
      <webElementGuid>f5ac592f-353d-4e9d-8632-6f1525e64d92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sepeda'])[1]/preceding::a[2]</value>
      <webElementGuid>8af3f9da-8dcc-474d-8a6b-956761bfcb96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/')]</value>
      <webElementGuid>8b21a1cd-9344-42a9-be51-59e1be9cc568</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>5421d77c-dca5-41ad-a58c-99096d1f134a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/' and (text() = ' ' or . = ' ')]</value>
      <webElementGuid>eb1c3836-f5ce-4ff4-b531-9a06cc20ff5e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
