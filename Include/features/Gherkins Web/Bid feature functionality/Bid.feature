Feature: Bid feature functionality
  user want to accept or reject bid from customer

  Scenario Outline: User swant to see the bid by notification
    Given I start the application
    When I go to login page
    And I input email
    And I input password
    And I click on masuk button
    When I click on notification button
    And I click on bid notification
    Then nothing happen still on the home page
    
  Scenario Outline: User want to accept and caonnect via whatsapp with the customer
    Given I start the application
    When I go to login page
    And I input email
    And I input password
    And I click on masuk button
    When I click on list button
    When I click on diminati list
    When I click on bid order
    And I click on accept bid order
    When I click on hubungi via whatsapp
    And I am directed to new window
    Then I can't connect with the customer
    
  Scenario Outline: User want to see the sold item on terjual list
    Given I start the application
    When I go to login page
    And I input email
    And I input password
    And I click on masuk button
    When I click on list button
    When I click on terjual button
    And I click on item on terjual page
    Then I see the item information on terjual page
    
  Scenario Outline: User want to reject the bid order after accept it and check the product back to all product page
    Given I start the application
    When I go to login page
    And I input email
    And I input password
    And I click on masuk button
    When I click on list button
    When I click on diminati button
    When I click on accept order accept order
    And I click on status button
    And I click on batalkan button
    And I click on kirim button
    When I click on back button
    When I click on terjual button
    When I click on all product button
    And I click on item on all product page
    Then I can see the item information on all product page
    
  Scenario Outline: User want to reject the bid and then change decision to no reject it
    Given I start the application
    When I go to login page
    And I input email
    And I input password
    And I click on masuk button
    When I click on list button
    When I click on diminati button
    When I click on bid order item
    And I click on reject button
    And I cllick on tidak button
    Then I see the item not rejected
    
  Scenario Outline: User want to reject the bid
    Given I start the application
    When I go to login page
    And I input email
    And I input password
    And I click on masuk button
    When I click on list button
    When I click on diminati button
    When I click on bid order item
    And I click on reject button
    And I cllick on iya button
    Then I see pop up notification tawaran ditolak
   
    
    
    
    