import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Open Website'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Sign Up/Open Website'), null, FailureHandling.STOP_ON_FAILURE)

'go to Sign Up'
WebUI.callTestCase(findTestCase("Test Cases/Page Web/Sign Up/Open Sign Up"), null, FailureHandling.STOP_ON_FAILURE)

'generate new email'
newEmail = WebUI.callTestCase(findTestCase('Test Cases/Page Web/Sign Up/Generate Email'), null, FailureHandling.STOP_ON_FAILURE)

'input Data'
print newEmail
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Sign Up/Input Data'), [('email') : 'torrac0612@gmail.com'], FailureHandling.STOP_ON_FAILURE)

'verify element email already used'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Sign Up/Verify Element Email Sudah Digunakan'), null, FailureHandling.STOP_ON_FAILURE)

'close browser'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Sign Up/Close Website'), null, FailureHandling.STOP_ON_FAILURE)