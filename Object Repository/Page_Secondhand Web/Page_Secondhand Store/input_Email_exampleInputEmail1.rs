<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Email_exampleInputEmail1</name>
   <tag></tag>
   <elementGuidId>b9579b84-f76b-4ee8-83fc-6f83b9d3e1e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#exampleInputEmail1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='exampleInputEmail1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>02e251c3-6a42-42b3-8ea4-70812854e9be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>02b4f25d-a7e0-4f3a-8ee8-4557787331a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>08d9cf09-b7d4-4cdf-ba58-35db0088761f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>exampleInputEmail1</value>
      <webElementGuid>ac01923f-cc0c-41f4-a2ba-d2ebf2f51570</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Contoh: johndee@gmail.com</value>
      <webElementGuid>91c4bdf2-3dfc-4cb1-b583-3e38b6201ac2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;exampleInputEmail1&quot;)</value>
      <webElementGuid>bf99acf1-9abb-4b91-8fd2-b70a1654a231</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='exampleInputEmail1']</value>
      <webElementGuid>58e06c3c-8a9c-42d7-9a04-61ff56a9a867</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div/input</value>
      <webElementGuid>48c4c28b-a10f-4788-8d4e-82038edcffe7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>cef8d49f-f5f6-4074-a6d5-a98988a1f73c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'email' and @id = 'exampleInputEmail1' and @placeholder = 'Contoh: johndee@gmail.com']</value>
      <webElementGuid>c82f934c-b942-45e7-8046-963ba503e6be</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
