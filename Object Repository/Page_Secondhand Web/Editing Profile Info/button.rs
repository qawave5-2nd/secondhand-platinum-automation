<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button</name>
   <tag></tag>
   <elementGuidId>280d9aaa-c4f6-4197-bc52-35e71d34eae3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/avatar-image/div[2]/div/div[2]/button[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>54c44ff2-eda2-42a6-8ec9-12e31d46680d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container mt-5&quot;]/div[1]/avatar-image[1]/div[2]/div[1]/div[2]/button[2]</value>
      <webElementGuid>2fbfc04d-81ea-41e0-9602-7ecb9b40d900</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>be2d1e55-7bc6-4f6a-ac95-ce140c624aeb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container mt-5&quot;]/div[1]/avatar-image[1]/div[2]/div[1]/div[2]/button[2]</value>
      <webElementGuid>ed8b1f14-cfb5-45ef-b78b-af5caa466b4e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/avatar-image/div[2]/div/div[2]/button[2]</value>
      <webElementGuid>6629bf06-e32a-4d26-90c8-4aaa74f52599</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log Out'])[1]/following::button[2]</value>
      <webElementGuid>9d1b6106-70c9-4b3e-8ded-eff41fd20434</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profile'])[1]/following::button[3]</value>
      <webElementGuid>8ed3a750-e7cd-4d31-b81b-4b785d441c0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama*'])[1]/preceding::button[1]</value>
      <webElementGuid>3c608f26-dcc0-4160-975f-ae3a2c4560cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kota*'])[1]/preceding::button[1]</value>
      <webElementGuid>1551c5fb-2041-42a0-8c06-c49f6edd28d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]</value>
      <webElementGuid>7e946fdc-4642-435d-b993-c52ab2158420</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
