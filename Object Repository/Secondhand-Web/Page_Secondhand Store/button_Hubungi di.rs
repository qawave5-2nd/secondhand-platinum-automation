<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Hubungi di</name>
   <tag></tag>
   <elementGuidId>23b5d655-663b-401a-bd0d-fe07b92e371b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>8e506958-7e22-4a8b-ba63-110fb6f28ec5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5c101015-892d-42f4-92f4-fda2184128ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bs-toggle</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>8e7af736-b956-457c-8ff7-70be2266d363</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bs-target</name>
      <type>Main</type>
      <value>#exampleModalTerima</value>
      <webElementGuid>a4013eec-4acf-4a15-bf31-b8c39dcd35e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary col btn btn-primary</value>
      <webElementGuid>6f1ac900-27e4-48ff-8b08-34d206fc1d28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Hubungi di </value>
      <webElementGuid>2793a740-0585-4040-a66e-487c6f4eb84e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;row mt-5&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;row mt-3&quot;]/div[@class=&quot;col-md-10&quot;]/div[@class=&quot;row mb-3&quot;]/div[@class=&quot;col-md-8 offset-md-4 row&quot;]/button[@class=&quot;btn btn-primary col btn btn-primary&quot;]</value>
      <webElementGuid>fa37b856-2cb7-4d18-8fbe-337c4258b810</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>751fa745-72d2-43fe-b14a-bbe00a762e8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>28fc15f4-742a-430b-b506-2ce477bf1fbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bs-toggle</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>ac9a0aa3-0f4b-4add-a940-ac3b998cde3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bs-target</name>
      <type>Main</type>
      <value>#exampleModalTerima</value>
      <webElementGuid>594d0ffb-2c10-4649-a58a-5e0d0b834d52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary col btn btn-primary</value>
      <webElementGuid>92d25580-7a7c-43b3-ae3b-a7e3e9acab09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Hubungi di </value>
      <webElementGuid>17f3d370-71a0-44f4-b48e-5a16ae6e8fa3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;row mt-5&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;row mt-3&quot;]/div[@class=&quot;col-md-10&quot;]/div[@class=&quot;row mb-3&quot;]/div[@class=&quot;col-md-8 offset-md-4 row&quot;]/button[@class=&quot;btn btn-primary col btn btn-primary&quot;]</value>
      <webElementGuid>82fea74f-6bc1-49f3-b67c-46c24a5aa587</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[4]</value>
      <webElementGuid>68b580e9-a23a-4e9c-adbd-1f8eabbb5cfa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[2]/div[2]/div[2]/div[2]/div/button[2]</value>
      <webElementGuid>bab420e6-60ba-404d-a8ec-4d3106ae4f83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/following::button[1]</value>
      <webElementGuid>7de52ed1-65ea-4695-a4c2-edbf813df11e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ditawar Rp 100.000,00'])[2]/following::button[2]</value>
      <webElementGuid>f0ead46b-4cdf-4710-910a-f4a6eeec3658</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Hubungi di']/parent::*</value>
      <webElementGuid>c0e9d4d4-6a30-4515-8e06-15d7284d6e7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]</value>
      <webElementGuid>e605c616-bcb2-4bc7-83d3-6d09ec23d970</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Hubungi di ' or . = 'Hubungi di ')]</value>
      <webElementGuid>d881c513-7242-4108-9b99-c0a7ade23756</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ditawar Rp 1.000.000,00'])[4]/following::button[2]</value>
      <webElementGuid>5102da5a-2dd5-4562-96c0-08c54fdeba87</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
