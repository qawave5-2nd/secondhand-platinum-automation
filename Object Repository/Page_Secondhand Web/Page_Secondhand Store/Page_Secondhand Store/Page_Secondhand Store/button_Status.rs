<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Status</name>
   <tag></tag>
   <elementGuidId>7d75d0b1-2bcb-43cd-8994-54354811a26e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-outline-primary.col.me-2.btn.btn-primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>db51c037-ad44-4455-940b-75656d94355d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7c83c6b4-3c7b-4e65-aa1d-40a2412f39b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-outline-primary col me-2 btn btn-primary</value>
      <webElementGuid>02091e9f-5bcc-4791-ac8d-d9925520edf6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Status</value>
      <webElementGuid>d0030f2f-6341-4ef3-bfcf-ab02504c7670</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;row mt-5&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;row mt-3&quot;]/div[@class=&quot;col-md-10&quot;]/div[@class=&quot;row mb-3&quot;]/div[@class=&quot;col-md-8 offset-md-4 row&quot;]/button[@class=&quot;btn btn-outline-primary col me-2 btn btn-primary&quot;]</value>
      <webElementGuid>e0da4df1-1186-42d7-9cec-0265ad153a2a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[3]</value>
      <webElementGuid>a052ac1d-8833-4fc2-8152-b54e30d0713c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[2]/div[2]/div[2]/div[2]/div/button</value>
      <webElementGuid>10aa27a0-cb93-47fb-900b-f8fbff3f892e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ditawar Rp 1.000.000,00'])[4]/following::button[1]</value>
      <webElementGuid>261bc723-748c-405d-b9d7-346208631ab6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 1.500.000,00'])[4]/following::button[1]</value>
      <webElementGuid>11416771-22f9-4659-9eac-296c9eb4b5b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hubungi di'])[1]/preceding::button[1]</value>
      <webElementGuid>6ec5bc41-0ecb-4cde-975b-acf64cd13c32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Status']/parent::*</value>
      <webElementGuid>287569d1-eedd-46ce-9cd7-5c01d243e804</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/button</value>
      <webElementGuid>ef6e5238-b5a0-4655-94b9-ace076cf22f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Status' or . = 'Status')]</value>
      <webElementGuid>e709de48-0df9-472f-ad78-0e9b6a01ab8c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
