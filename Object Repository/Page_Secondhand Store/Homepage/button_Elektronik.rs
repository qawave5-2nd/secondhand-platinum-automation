<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Elektronik</name>
   <tag></tag>
   <elementGuidId>6f05a593-286e-4237-8329-dfaef6d60f35</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>26e253ff-7417-49d8-98ad-1664f9ed2644</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9c0edee8-cecd-4746-8225-4276cac1a75d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-color-theme pl-3 pr-3</value>
      <webElementGuid>9a3ad5d4-1361-4f30-a549-1e2d08f2d36f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Elektronik</value>
      <webElementGuid>085139c9-8dc7-4a11-b5c0-677f19d12532</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;pb-5&quot;]/div[@class=&quot;container pb-5&quot;]/div[@class=&quot;category&quot;]/div[@class=&quot;category__list&quot;]/button[@class=&quot;btn btn-color-theme pl-3 pr-3&quot;]</value>
      <webElementGuid>e7d8e6d0-14c8-475b-a368-31ef64aa040f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[5]</value>
      <webElementGuid>cb400a91-dee5-407c-b321-6844bbf427e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/div/div/button[5]</value>
      <webElementGuid>aa53b213-82ed-49a3-9b60-4c4d1ffcc81c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jual'])[1]/preceding::button[2]</value>
      <webElementGuid>35edd135-3a01-47a2-8ac1-b4b1979b6a67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Elektronik']/parent::*</value>
      <webElementGuid>f1d8c553-bf64-411e-a378-9fe8ae0d07fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[5]</value>
      <webElementGuid>cb0e57b1-21bb-4c0f-a9db-7b7222d84fe7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = ' Elektronik' or . = ' Elektronik')]</value>
      <webElementGuid>8305a4ab-8846-4691-a035-7f0c1e5573ad</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
