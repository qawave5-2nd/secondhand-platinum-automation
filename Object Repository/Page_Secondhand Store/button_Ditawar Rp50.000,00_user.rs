<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Ditawar Rp50.000,00_user</name>
   <tag></tag>
   <elementGuidId>5df572b0-d5e3-4929-b6f1-568a33dcaefd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#user</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='user']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>08eefffb-1cc2-408a-8156-39c400a3ba2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user</value>
      <webElementGuid>02a89bf7-aeda-4d38-ba11-fd29cf0f6c0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9e283a0f-3c39-407f-8993-93a424dbb65a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-link</value>
      <webElementGuid>9cee7929-fe07-427b-a553-0e0fcf835b51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>6e789729-b2b9-488a-93d5-3782f34cdfec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-display</name>
      <type>Main</type>
      <value>static</value>
      <webElementGuid>c5011371-3c91-4013-8219-068d34db1a3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>c65cece6-0605-45bb-90b4-8f3936572381</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user&quot;)</value>
      <webElementGuid>6db66d78-957e-4e48-8f26-f1fe4d54803c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='user']</value>
      <webElementGuid>e9e0a974-62ef-452a-9919-b8c8bd96134e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarText']/ul/li[3]/button</value>
      <webElementGuid>3e3ae35c-adb5-425d-bb07-711100756709</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ditawar Rp 50.000,00'])[1]/following::button[1]</value>
      <webElementGuid>5951d690-447b-4b9e-989b-f4426fb32846</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 75.000,00'])[1]/following::button[1]</value>
      <webElementGuid>c1978f1f-d1ca-45a6-85ba-23f6d0e2de9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profile'])[1]/preceding::button[1]</value>
      <webElementGuid>5dbc5db3-4a7c-4f20-bd5b-88fcc04c2c84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log Out'])[1]/preceding::button[1]</value>
      <webElementGuid>966a6731-12b3-4f28-bbfd-6af3244feec9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/button</value>
      <webElementGuid>ace134fd-2126-4e4d-980a-4f7839dbc5d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'user' and @type = 'button']</value>
      <webElementGuid>4fb8c37e-d28a-4842-bced-17c3a23c6a53</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
