Feature: Notification 
User check notification tab after adding product in market.

Rule: -

	Scenario : Check notification Functionality
		Given I am launch the apps
		When I click notification tab
		And I click notification
		Then I see unresponsive page
		
Rule : Only User after login successfull can see it.

	Scenario : Check notification Functionality. User check notification tab after user product in market get offered by another user.
		Given I am launch the app
		When I go to login page
		And I click masuk button
		When I input user data
		And I input Email
		And I input Password
		And I click masuk button
		When I click notification tab
		And I click notification
		And I click product = "baju Baru"
		Then I see unresponsive page