Feature: Check Register Functionality
User able to input data when registered.

Rule: Only using valid email.

	Scenario : User want to register with registered email.
		Given I am launch the app
		When I go to login page
		And I click daftar button
		When I input user data
		And I input Nama "Nurul Dewa Basoka"
		And I input Email "nurulbasoka1@mail.com"
		And I input Password "asdasdasd"
		And I input Number Phone "081231387123"
		And I input City "Split"
		And I input Alamat "JL. Hqajiasdihjkgad"
		And I click daftar button
		Then I see message <You must verification email>
		

	Scenario : User want to register with registered email.
		Given I am launch the app
		When I go to login page
		And I click daftar button
		When I input user data
		And I input Nama "Nurul Dewa Basoka"
		And I input Email "nurulbasoka1@mail.com"
		And I input Password "asdasdasd"
		And I input Number Phone "081231387123"
		And I input City "Split"
		And I input Alamat "JL. Hqajiasdihjkgad"
		And I click daftar button
		Then I see message <Email sudah digunakan>
		

	Scenario : User want to register but not input name field.
		Given I am launch the app
		When I go to login page
		And I click daftar button
		When I input user data
		And I input Email "nurulbasoka1@mail.com"
		And I input Password "asdasdasd"
		And I input Number Phone "081231387123"
		And I input City "Split"
		And I input Alamat "JL. Hqajiasdihjkgad"
		And I click daftar button
		Then I see message <Nama tidak boleh kosong>
		

	Scenario : User want to register but not input email field.
		Given I am launch the app
		When I go to login page
		And I click daftar button
		When I input user data
		And I input Nama "Nurul Dewa Basoka"
		And I input Password "asdasdasd"
		And I input Number Phone "081231387123"
		And I input City "Split"
		And I input Alamat "JL. Hqajiasdihjkgad"
		And I click daftar button
		Then I see message <Email tidak boleh kosong>
		

	Scenario : User want to register but input not valid email.
		Given I am launch the app
		When I go to login page
		And I click daftar button
		When I input user data
		And I input Nama "Nurul Dewa Basoka"
		And I input Email "nurulbasoka1"
		And I input Password "asdasdasd"
		And I input Number Phone "081231387123"
		And I input City "Split"
		And I input Alamat "JL. Hqajiasdihjkgad"
		And I click daftar button
		Then I see message <Email tidak valid>
		

	Scenario : User want to register but not input password field.
	Given I am launch the app
		When I go to login page
		And I click daftar button
		When I input user data
		And I input Nama "Nurul Dewa Basoka"
		And I input Email "nurulbasoka1@mail.com" 
		And I input Number Phone "081231387123"
		And I input City "Split"
		And I input Alamat "JL. Hqajiasdihjkgad"
		And I click daftar button
		Then I see message <Password tidak boleh kosong>
	
	Scenario : User want to register but not input number phone field.
	Given I am launch the app
		When I go to login page
		And I click daftar button
		When I input user data
		And I input Nama "Nurul Dewa Basoka"
		And I input Email "nurulbasoka1@mail.com" 
		And I input Password "asdasdasd" 
		And I input City "Split"
		And I input Alamat "JL. Hqajiasdihjkgad"
		And I click daftar button
		Then I see message <Nomor Telepon tidak boleh kosong>
		

	Scenario : User want to register but not input city field.
	Given I am launch the app
		When I go to login page
		And I click daftar button
		When I input user data
		And I input Nama "Nurul Dewa Basoka"
		And I input Email "nurulbasoka1@mail.com" 
		And I input Password "asdasdasd" 
		And I input Number Phone "0812312414"
		And I input Alamat "JL. Hqajiasdihjkgad"
		And I click daftar button
		Then I see message <Kota tidak boleh kosong>
		

	Scenario : User want to register but not input alamat field.
	Given I am launch the app
		When I go to login page
		And I click daftar button
		When I input user data
		And I input Nama "Nurul Dewa Basoka"
		And I input Email "nurulbasoka1@mail.com" 
		And I input Password "asdasdasd" 
		And I input Number Phone "0812312414"
		And I input City "Split" 
		And I click daftar button
		Then I see message <Alamat tidak boleh kosong>