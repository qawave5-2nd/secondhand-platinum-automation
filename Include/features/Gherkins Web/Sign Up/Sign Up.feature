Feature: Sign Up
User want to register new account.

Rule: Only Unregistered email can be register to make new account

	Scenario : User want to sign up to make new account after entering the website.
		Given I am open the website
		When I go to login page
		And I click Daftar disini button
		When I at register page
		And I input new username
		And I input new unregistered email
		And I input password
		And I click Daftar
		Then I should see "Please verify email to use our service."
		
	Scenario : User want to sign up to make new account after entering the website using Registered Email.
		Given I am open the website
		When I go to login page
		And I click Daftar disini button
		When I at register page
		And I input new username
		And I input registered email
		And I input password
		And I click Daftar
		Then I should see "Email already Used."