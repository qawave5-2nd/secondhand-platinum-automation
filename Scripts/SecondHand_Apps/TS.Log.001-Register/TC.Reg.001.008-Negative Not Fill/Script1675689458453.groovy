import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Open Application'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-General/Start Application'), null, FailureHandling.STOP_ON_FAILURE)

'Masuk Halaman Daftar'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Register/Register Page'), null, FailureHandling.STOP_ON_FAILURE)

'Input Data Negative'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Register/Input Data - Negative - 5'), null, FailureHandling.STOP_ON_FAILURE)

'Klik Button Daftar'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Register/Click Button Daftar'), null, FailureHandling.STOP_ON_FAILURE)

'Verify Pop up'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Register/Verify Pop Up Number Phone'), null, FailureHandling.STOP_ON_FAILURE)

'Close Application'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-General/Close Application'), null, FailureHandling.STOP_ON_FAILURE)


