import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Call Go to Profile Edit Page'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Account Functionality/Go to Profile Edit Page'), null, FailureHandling.STOP_ON_FAILURE)

'Tap Edit Phone Number'
Mobile.tap(findTestObject('Object Repository/Secondhand_App/Account Functionality/android.widget.ImageView (7)'), 0)

'Call Generate New Number'
newNumber = Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Account Functionality/Generate Phone Number Page'), null, FailureHandling.STOP_ON_FAILURE)

'Set New Phone Number'
Mobile.setText(findTestObject('Object Repository/Secondhand_App/Account Functionality/android.widget.EditText - 64727'), newNumber, 0)

'Tap Simpan Button'
Mobile.tap(findTestObject('Object Repository/Secondhand_App/Account Functionality/android.widget.Button - Simpan (1)'), 0)

'Call Verify Profile Update Success Page'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Account Functionality/Verify Profile Update Success Page'), null, FailureHandling.STOP_ON_FAILURE)

'Call Close App page'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Account Functionality/Close App Page'), null, FailureHandling.STOP_ON_FAILURE)
