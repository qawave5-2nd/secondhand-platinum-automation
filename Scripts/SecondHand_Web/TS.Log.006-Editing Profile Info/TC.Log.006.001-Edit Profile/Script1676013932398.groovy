import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Call Login Account Page'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Editing Profile Info/Login Account Page'), null, FailureHandling.STOP_ON_FAILURE)

'Call Go to Profile Info Page'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Editing Profile Info/Go to Profile Info'), null, FailureHandling.STOP_ON_FAILURE)

'Set New Photo'
WebUI.uploadFile(findTestObject('Object Repository/Page_Secondhand Web/Editing Profile Info/profile pict'), 'C:\\Users\\destr\\Downloads\\rokok.jpg')

'Click Button Checklist'
WebUI.click(findTestObject('Object Repository/Page_Secondhand Web/Editing Profile Info/button_submit'))

'Call Generate Name Page'
newName = WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Editing Profile Info/Name Generator Page'), null, FailureHandling.STOP_ON_FAILURE)

'Set New Name'
WebUI.setText(findTestObject('Object Repository/Page_Secondhand Web/Editing Profile Info/input_Nama_nm_produk'), newName,)

'Pick New Town'
WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Secondhand Web/Editing Profile Info/select_Pilih kotaBandungBogorJemberKediriLu_56995e'),
	'Jember', true)

'Call Generate Address Page'
newAddress = WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Editing Profile Info/Address Generator Page'), null, FailureHandling.STOP_ON_FAILURE)

'Set New Address'
WebUI.setText(findTestObject('Object Repository/Page_Secondhand Web/Editing Profile Info/textarea_wdqweqwe'), newAddress,)

'Call Generate Phone Page'
newPhone = WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Editing Profile Info/Phone Generator Page'), null, FailureHandling.STOP_ON_FAILURE)

'Set New Phone Number'
WebUI.setText(findTestObject('Object Repository/Page_Secondhand Web/Editing Profile Info/input_No.Handphone_nm_produk'), newPhone,)

'Click Simpan Button'
WebUI.click(findTestObject('Object Repository/Page_Secondhand Web/Editing Profile Info/button_submit'))

'Verify Success to Update Profile'
WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Secondhand Web/Editing Profile Info/p_Berhasil update profile'))

'Call Close Url Page'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-General/Close Browser'), null, FailureHandling.STOP_ON_FAILURE)