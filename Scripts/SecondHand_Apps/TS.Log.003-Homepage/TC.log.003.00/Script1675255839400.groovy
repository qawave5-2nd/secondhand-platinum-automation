import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
'Start Application'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-General/Start Application'), null, FailureHandling.STOP_ON_FAILURE)

'Tap Search Box'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Homepage/Tap Search Box'), null, FailureHandling.STOP_ON_FAILURE)

'input text "Makanan Untuk Makan Siang"'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Homepage/Input Data Search Box'), null, FailureHandling.STOP_ON_FAILURE)

'Tap the product'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Homepage/Tap the Product'), null, FailureHandling.STOP_ON_FAILURE)

'search result should be same with input'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-Homepage/Verify Name'), null, FailureHandling.STOP_ON_FAILURE)

'Close Application'
Mobile.callTestCase(findTestCase('Test Cases/Page App/Page-General/Close Application'), null, FailureHandling.STOP_ON_FAILURE)

