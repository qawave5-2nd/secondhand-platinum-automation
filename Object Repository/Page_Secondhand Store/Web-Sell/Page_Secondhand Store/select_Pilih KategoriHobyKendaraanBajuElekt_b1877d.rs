<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Pilih KategoriHobyKendaraanBajuElekt_b1877d</name>
   <tag></tag>
   <elementGuidId>81133078-c6de-4deb-9398-7691e0af26ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='kategori']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#kategori</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>fa206404-e9d2-49e3-9f51-f349ea927d27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>4f820446-6c07-449e-a1bc-6ebe5cbb25c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>kategori</value>
      <webElementGuid>68f8def5-cca8-4642-be55-fa80e0bb640e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih KategoriHobyKendaraanBajuElektronikKesehatan</value>
      <webElementGuid>8a517874-bf10-40eb-876e-0e4ebc0a291f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;kategori&quot;)</value>
      <webElementGuid>dbf93b6c-ba39-4695-bda5-351f36ac85ae</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='kategori']</value>
      <webElementGuid>d85010d1-05ab-49d7-84f1-138185253e18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/form/div[3]/select</value>
      <webElementGuid>49a8281c-3971-4ac4-99eb-830542c92114</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::select[1]</value>
      <webElementGuid>ed41efec-2619-4c52-9431-dcea14537fba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga Produk'])[1]/following::select[1]</value>
      <webElementGuid>ff3d1bf7-1fbd-4f24-9f40-0cd37d0c5986</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/preceding::select[1]</value>
      <webElementGuid>b0b80bb4-796f-4177-aae9-81bb61ecda50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Foto Produk'])[1]/preceding::select[1]</value>
      <webElementGuid>f0d4f467-0370-4230-a3d7-65a54d239639</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>c848ee0b-7df8-49bc-8c6d-b0d5c33c6304</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'kategori' and (text() = 'Pilih KategoriHobyKendaraanBajuElektronikKesehatan' or . = 'Pilih KategoriHobyKendaraanBajuElektronikKesehatan')]</value>
      <webElementGuid>5d415a10-ae0e-486e-b28b-dec3ead16e29</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
