<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_Pencil</name>
   <tag></tag>
   <elementGuidId>c968efc4-a697-4bdb-8711-2ae4dd77448f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#deskripsi</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='deskripsi']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>a4f35b46-1a10-491b-993b-546a1b3aa938</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>008cdaac-b9b6-4cca-8daf-a090d9d2de16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>deskripsi</value>
      <webElementGuid>4417a477-064e-483a-b3cd-fae2574a1d18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>c7b62b5f-719d-4a3e-b58b-efcdbf212040</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Contoh: Jalan Ikan Hiu 33</value>
      <webElementGuid>0ffb08a9-49ba-4ac1-8ae0-1dabfdc45522</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pencil</value>
      <webElementGuid>dce159b8-dd62-466a-b390-428d03c999d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;deskripsi&quot;)</value>
      <webElementGuid>bfe14ca3-27de-49e3-b980-7b83319c487a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='deskripsi']</value>
      <webElementGuid>850a9c10-c9c4-44da-a9e6-43ebb2ac5213</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/form/div[4]/textarea</value>
      <webElementGuid>0d58c976-cc82-428b-99be-b471d3006ae3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::textarea[1]</value>
      <webElementGuid>34555950-2f76-45f6-8b59-3ecb76286c96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::textarea[1]</value>
      <webElementGuid>061a374f-93e0-4847-99ed-4bb65e84d512</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Foto Produk'])[1]/preceding::textarea[1]</value>
      <webElementGuid>707ab5a6-3b04-4fdb-8f62-5aa0a0423d3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/preceding::textarea[1]</value>
      <webElementGuid>e9bf1b96-14c5-4176-a9dd-a16d74ebd38a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>85011532-ea08-4e5e-98e2-b644e987f209</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'deskripsi' and @placeholder = 'Contoh: Jalan Ikan Hiu 33' and (text() = 'Pencil' or . = 'Pencil')]</value>
      <webElementGuid>ef0bee6a-ee2d-4e1f-8de8-8a6fe803f7af</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
