import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('F:\\Fd-QAE-Anam\\apk-secondhand\\app-release.apk', false)

Mobile.tap(findTestObject('Object Repository/Secondhand_App/My Order/android.widget.TextView - Akun'), 0)

Mobile.tap(findTestObject('Object Repository/Secondhand_App/My Order/android.widget.TextView - Pesanan Saya'), 0)

Mobile.verifyElementText(findTestObject('Object Repository/Secondhand_App/My Order/android.widget.TextView - Baju'), 'Baju')

Mobile.verifyElementText(findTestObject('Object Repository/Secondhand_App/My Order/android.widget.TextView - Menunggu'), 
    'Menunggu')

Mobile.verifyElementText(findTestObject('Object Repository/Secondhand_App/My Order/android.widget.TextView - Boneka'), 'Boneka')

Mobile.verifyElementText(findTestObject('Object Repository/Secondhand_App/My Order/android.widget.TextView - Menunggu (1)'), 
    'Menunggu')

Mobile.verifyElementText(findTestObject('Object Repository/Secondhand_App/My Order/android.widget.TextView - AK-47'), 'AK-47')

Mobile.verifyElementText(findTestObject('Object Repository/Secondhand_App/My Order/android.widget.TextView - Dibatalkan'), 
    'Dibatalkan')

Mobile.closeApplication()

