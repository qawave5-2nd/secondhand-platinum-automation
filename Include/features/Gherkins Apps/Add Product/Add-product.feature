Feature: Add product functionality
  User want to add product and try all the feature
  function on the add product page 
  
  Rule: Only registered user can add the product
  
  Scenario Outline: User switch the phone to landscpe view
    Given I start the application
    When I go to login page
    And I click on login button
    And I input the data
    And I click on masuk button
    When I click on add product button
    And I am directed to the add product page
    When I switch the screen to landscape view
    Then I automatically exit from add product page
    And I verified it as I am directed to the home page
 	
 	Scenario Outline: User want to exit from add product page
    Given I start the application
    When I go to login page
    And I click on login button
    And I input the data
    And I click on masuk button
    When I click on add product button
    And I am directed to the add product page
    When I click on back button
    Then I'am directed to the home page
  
  Scenario Outline: User want to preview the product before add it on the store
    Given I start the application
    When I go to login page
    And I click on login button
    And I input the data
    And I click on masuk button
    When I click on add product button
    And I am directed to the add product page
    When I input product data
    And I input product name
    And I input product price
    And I input product category
    And I input the product location
    And I input the product description
    And I input the product picturet
    When I click on preview button
    Then I see preview of the product
  
  Scenario Outline: User want to add the product to the store
    Given I start the application
    When I go to login page
    And I click on login button
    And I input the data
    And I click on masuk button
    When I click on add product button
    And I am directed to the add product page
    When I input product data
    And I input product name
    And I input product price
    And I input product category
    And I input the product location
    And I input the product description
    And I input the product picture
    When I click on terbitkan button
    Then I directed to my added product
  
  Scenario Outline: User want to add more than 5 products to the store
    Given I start the application
    When I go to login page
    And I click on login button
    And I input the data
    And I click on masuk button
    When I click on add product button
    And I am directed to the add product page
    When I input product data
    And I input product name
    And I input product price
    And I input product category
    And I input the product location
    And I input the product description
    And I input the product picturet
    When I click on terbitkan button
    Then I see error notification said "kamu sudah menerbitkan 5 product"
   
  Scenario Outline: User want to add product to the store without filling name section
    Given I start the application
    When I go to login page
    And I click on login button
    And I input the data
    And I click on masuk button
    When I click on add product button
    And I am directed to the add product page
    When I input product data
    And I don't input product name
    And I input product price
    And I input product category
    And I input the product location
    And I input the product description
    And I input the product picture
    When I click on terbitkan button
    Then I see error notification
   
  Scenario Outline: User want to add product to the store without filling price section
    Given I start the application
    When I go to login page
    And I click on login button
    And I input the data
    And I click on masuk button
    When I click on add product button
    And I am directed to the add product page
    When I input product data
    And I input product name
    And I don't input product price
    And I input product category
    And I input the product location
    And I input the product description
    And I input the product picture
    When I click on terbitkan button
    Then I see error notification
    
  Scenario Outline: User want to add product to the store without filling category section
    Given I start the application
    When I go to login page
    And I click on login button
    And I input the data
    And I click on masuk button
    When I click on add product button
    And I am directed to the add product page
    When I input product data
    And I input product name
    And I input product price
    And I don't input product category
    And I input the product location
    And I input the product description
    And I input the product picture
    When I click on terbitkan button
    Then I see error notification
    
  Scenario Outline: User want to add product to the store without filling location section
    Given I start the application
    When I go to login page
    And I click on login button
    And I input the data
    And I click on masuk button
    When I click on add product button
    And I am directed to the add product page
    When I input product data
    And I input product name
    And I input product price
    And I input product category
    And I don't input the product location
    And I input the product description
    And I input the product picture
    When I click on terbitkan button
    Then I see error notification
    
  Scenario Outline: User want to add product to the store without filling description section
    Given I start the application
    When I go to login page
    And I click on login button
    And I input the data
    And I click on masuk button
    When I click on add product button
    And I am directed to the add product page
    When I input product data
    And I input product name
    And I input product price
    And I input product category
    And I input the product location
    And I don't input the product description
    And I input the product picture
    When I click on terbitkan button
    Then I see error notification
    
  Scenario Outline: User want to add product to the store without input picture of the product
    Given I start the application
    When I go to login page
    And I click on login button
    And I input the data
    And I click on masuk button
    When I click on add product button
    And I am directed to the add product page
    When I input product data
    And I input product name
    And I input product price
    And I input product category
    And I input the product location
    And I input the product description
    And I don't input the product picture
    When I click on terbitkan button
    Then I see error notification
    
    
    
    
    
    

 