<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>Spinner_content</name>
   <tag></tag>
   <elementGuidId>272f3551-4577-4950-a633-161e16d1de9c</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(@text, '${value}') and @text='${value}']</value>
      <webElementGuid>d0707850-799d-44ae-8ebd-5a3b2ada7901</webElementGuid>
   </webElementProperties>
   <locator></locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
