<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>android.widget.Toast - Email sudah digunakan (1)</name>
   <tag></tag>
   <elementGuidId>2e3af018-af5e-4746-8a08-4bf939acc5f2</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//hierarchy/android.widget.Toast[1]</value>
      <webElementGuid>c9a71657-c3ed-4ff1-8c66-01acaa702688</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Text</name>
      <type>Main</type>
      <value>Email sudah digunakan</value>
      <webElementGuid>500e30ba-882a-4612-8a50-ee79a6c338cc</webElementGuid>
   </webElementProperties>
   <locator>//hierarchy/android.widget.Toast[1][count(. | //*[(@text = 'Email sudah digunakan' or . = 'Email sudah digunakan')]) = count(//*[(@text = 'Email sudah digunakan' or . = 'Email sudah digunakan')])]</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
