Feature: Check Beranda Functionality
User able to click category in the homepage \.

Rule: Only filtered product in the category.

	Scenario : User want to click category Elektronik in the homepage.
		Given I am launch the app
		And I am logged in
		When I click category Elektronik
		Then I page category elektronik
		But Filtered always random category
		
		
	Scenario : User want to click category Elektronik in the homepage.
		Given I am launch the app
		And I am logged in
		When I click category Elektronik
		Then I page category elektronik
		But Filtered always random category
		
		
	Scenario : User want to login but not input email field.
		Given I am launch the app
		When I go to login page
		And I input user data
		And I input Password "asdasdasd"
		And I click daftar button
		Then I see message <Email tidak boleh kosong>
		
		
	Scenario : User want to login but not input email.
		Given I am launch the app
		When I go to login page
		And I input user data
		And I input Email "nurulbasoka11@mail.com"
		And I click daftar button
		Then I see message <Password atau kata sandi salah>
		
		