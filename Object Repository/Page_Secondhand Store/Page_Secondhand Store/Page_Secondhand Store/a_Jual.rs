<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Jual</name>
   <tag></tag>
   <elementGuidId>fcc13cbb-1200-47fb-81bf-03d7119ce6f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-color-theme.pl-3.pr-3.button-jual</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@type='button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>8ad37388-4b08-4ad7-993b-50c819b89139</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>705de046-94b8-455a-bd3c-607781638b0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-color-theme pl-3 pr-3 button-jual</value>
      <webElementGuid>b61b6ded-0066-4162-9c9a-d4b7cd77c678</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/productinfo</value>
      <webElementGuid>385f5ef5-c151-4c9a-af42-46f957c4d59e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Jual</value>
      <webElementGuid>b9e87b74-9816-4e70-82a3-644987a705d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/a[@class=&quot;btn btn-color-theme pl-3 pr-3 button-jual&quot;]</value>
      <webElementGuid>47710203-4928-4139-9101-810dad892a3d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@type='button']</value>
      <webElementGuid>ca82f9c3-a0c1-45b2-9f48-f258feb7297f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/a</value>
      <webElementGuid>0b83cff0-1142-408c-b817-b9c2ea57d937</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Jual')]</value>
      <webElementGuid>17b72b75-46ac-4e5f-93c7-d9098acacf43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/productinfo')]</value>
      <webElementGuid>3bfdc652-721b-42ba-9792-ef6e5c9650e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/a</value>
      <webElementGuid>5a840b97-84f6-40c3-8dee-f0fc08060342</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@type = 'button' and @href = '/productinfo' and (text() = ' Jual' or . = ' Jual')]</value>
      <webElementGuid>f2a1a985-d8df-403a-8019-b3dab3c80c8b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
