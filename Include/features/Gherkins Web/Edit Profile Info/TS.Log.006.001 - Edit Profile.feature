Feature: Edit Profile 
User able to editing profile info, Non registered user haven't profile info

Rule: Only registered user can edit the profile info

Scenario : User login with registered email than editing profile info
		Given I am open the website
		When I go to login account page
		And I click login button
		And I put registered email
		And I put password
		And I click masuk button
		When I go to profile info page
		And I click profile icon
		And I click profile
		When I input new profile info
		And I put new photo
		And I put new name
		And I pick new town
		And I put new address
		And I put new phone number
		When I submit new profile info
		And I click simpan button
		When I want verify the new profile info has updated
		Then I see pop-up notif appeared with the words 'success to update profile'