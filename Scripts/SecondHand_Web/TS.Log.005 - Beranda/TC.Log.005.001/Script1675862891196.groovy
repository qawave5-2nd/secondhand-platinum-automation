import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Open Browser And Go to The Url'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-General/Start Open URL'), null, FailureHandling.STOP_ON_FAILURE)

'Click Logo And Verify'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Click And Verify Logo Second Hand'), null, FailureHandling.STOP_ON_FAILURE)

'Click Category Semua'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Click Category Semua'), null, FailureHandling.STOP_ON_FAILURE)

'Verify Category Semua'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Verify Category Semua'), null, FailureHandling.STOP_ON_FAILURE)

'Click Category Hoby'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Click Category Hobby'), null, FailureHandling.STOP_ON_FAILURE)

'Verify Category Hoby'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Verify Category Hobby'), null, FailureHandling.STOP_ON_FAILURE)

'Click Category Kendaraan'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Click Category Kendaraan'), null, FailureHandling.STOP_ON_FAILURE)

'Verify Category Kendaraan'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Verify Category Kendaraan'), null, FailureHandling.STOP_ON_FAILURE)

'Click Category Baju'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Click Category Baju'), null, FailureHandling.STOP_ON_FAILURE)

'Verify Category Baju'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Verify Category Baju'), null, FailureHandling.STOP_ON_FAILURE)

'Click Category Elektronik'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Click Category Elektronik'), null, FailureHandling.STOP_ON_FAILURE)

'Verify Category Elektronik'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Verify Category Elektronik'), null, FailureHandling.STOP_ON_FAILURE)

'Click Category Kesehatan'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Click Category Kesehatan'), null, FailureHandling.STOP_ON_FAILURE)

'Verify Category Kesehatan'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-Homepage/Verify Category Kesehatan'), null, FailureHandling.STOP_ON_FAILURE)

'Close Browser'
WebUI.callTestCase(findTestCase('Test Cases/Page Web/Page-General/Close Browser'), null, FailureHandling.STOP_ON_FAILURE)

