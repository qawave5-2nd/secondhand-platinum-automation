import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.setText(findTestObject('Object Repository/Secondhand_App/Register/android.widget.EditText - Masukkan nama lengkap'), 'Nurul',
	0)

Mobile.setText(findTestObject('Object Repository/Secondhand_App/Register/android.widget.EditText - Masukkan email (3)'),('nurulbasoka1@mail.com'), 0)

Mobile.setEncryptedText(findTestObject('Object Repository/Secondhand_App/Register/android.widget.EditText - Masukkan password (3)'),
	'S2AZ2CswOAOlQb1Nd5D/WQ==', 0)

Mobile.setText(findTestObject('Object Repository/Secondhand_App/Register/android.widget.EditText - Contoh 08123456789'), '08437496485',
	0)

Mobile.setText(findTestObject('Object Repository/Secondhand_App/Register/android.widget.EditText - Masukkan kota'), '', 0)

Mobile.setText(findTestObject('Object Repository/Secondhand_App/Register/android.widget.EditText - Masukkan alamat'), 'Jl. Gang Hall No. A, Kec. Split, Kab, Bojong, Prov Palo',
	0)
